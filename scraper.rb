require 'httparty'
require 'nokogiri'
require 'json'

def get_month
  year = File.basename(Dir.glob('data/*.json').sort.first).to_i - 1
  year >= 2000 ? year : abort
end

def scrape_page page = 1
  url   = $url + "catalogo.php?mode=resultados_avanzada&fecha_aparicion=#{$date}&pagina=#{page}"
  res   = HTTParty.get(url)
  books = Nokogiri::HTML(res.body).css('.lista_libros .no-padding')

  if books.length > 0
    puts "Scraping #{$date}, page #{page}"
    add(books)
    scrape_page(page + 1)
  else
    change_date
  end
end

def add books
  books.each do |book|
    isbn = book.css('.isbn').first.content.gsub(/[^0-9-]/, '')
    keys = clean_keys(book.css('.labels'))
    vals = book.css('span:not(.isbn):not(.labels)')
    link = fix_url(vals.first.css('a').attribute('href'))
    vals = clean_vals(vals)
    obj  = {:isbn => isbn, :url => link}
    add_keys(obj, vals, keys)
    $all.push(obj)
  end
end

def clean_keys a
  return a.map{ |e| e.content.gsub(':', '').downcase }
end

def clean_vals a
  return a.map{ |e| e.content }
end

def fix_url s
  return s.to_s.length > 0 ? $url + s.to_s[2..-1] : nil
end

def add_keys o, v, kk
  kk.each_with_index do |k, i|
    o[k.to_sym] = v[i] ? v[i] : ''
  end
end

def change_date
  save
end

def save
  name = 'data/' + $date.year.to_s + '.json'
  file = File.open(name, 'w')
  file.puts JSON.pretty_generate($all)
  file.close
  if $date.year != $date.prev_day.year
    system("git add data && git commit -m 'Guardado de #{$date.year}-#{$date.month}' && git push origin master")
  else continue end
end

def continue
  $date = $date.prev_day
  scrape_page
end

$url  = 'https://isbnmexico.indautor.cerlalc.org/'
$date = Date.new(get_month, 12, 31)
$all  = []

scrape_page
